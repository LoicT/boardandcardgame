import { Injectable } from '@nestjs/common';
import { Game } from 'src/entities/game.entity';
import { Role } from 'src/entities/role.entity';
import { Connection, Repository } from 'typeorm';
import { NbRole } from 'src/entities/nbRole.entity';
import { RoleInGame } from 'src/entities/roleInGame.entity';

@Injectable()
export class RoleService {

  gameRepository: Repository<Game>;
  nbRoleRepository: Repository<NbRole>;
  rolesInGameRepository: Repository<RoleInGame>;

  constructor(
    private connection: Connection
    ) {
    this.gameRepository = connection.getRepository(Game);
    this.nbRoleRepository = connection.getRepository(NbRole);
    this.rolesInGameRepository = connection.getRepository(RoleInGame);
  }

  async distributeRole(game: Game): Promise<Game> {
    let roles: Role[] = [];
    const tempRole = await this.nbRoleRepository.find({where:{game:game.id}, relations:["role", "game"]});
    game.rolesingame = await this.rolesInGameRepository.find({where:{game:game.id}, relations:["role", "game", "user"]})
    for (const nbRole of tempRole) {
      for (let i = 0; i < nbRole.count; i++) {
        roles.push(nbRole.role);
      }
    }
    this.shuffleArray(roles);
    for (let i = 0; i < game.rolesingame.length; i++) {
      game.rolesingame[i].role = roles[i];
    }
    return game;
  }

  private shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
}
}
