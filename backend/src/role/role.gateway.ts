import { SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server } from 'socket.io';
import { NbRole } from 'src/entities/nbRole.entity';

@WebSocketGateway()
export class RoleGateway {

  @WebSocketServer()
  server: Server;
  
  @SubscribeMessage('message')
  handleMessage(client: any, payload: any): string {
    return 'Hello world!';
  }

  changeNbRole(nbRole: NbRole){
    this.server.emit("changeNbRole", nbRole);
  }
}
