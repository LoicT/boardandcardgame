import { Controller, Get, Post, Req, Param } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { Role } from 'src/entities/role.entity';
import { NbRole } from 'src/entities/nbRole.entity';
import { Game } from 'src/entities/game.entity';
import { RoleGateway } from './role.gateway';

@Controller('roles')
export class RoleController {

  private roleRepository: Repository<Role>
  nbRoleRepository: Repository<NbRole>;

  constructor(
    private connection: Connection,
    private roleGateway: RoleGateway
  ){
    this.roleRepository = this.connection.getRepository(Role);
    this.nbRoleRepository = connection.getRepository(NbRole);
  }

  @Get()
  getAll(){
    return this.roleRepository.find();
  }

  @Get("/:id")
  getById(@Param() params){
    return this.roleRepository.findOne(params.id);
  }

  @Post()
  postNewRole(@Req() request){
    return this.roleRepository.save(request.body);
  }

  @Post("/:id/game/:idGame/count/:count")
  async addRole(@Param() params){
    const nbRole = new NbRole(new Role(params.id), new Game(params.idGame), params.count)
    let find = await this.nbRoleRepository.findOne({where:{game:params.idGame, role:params.id}, relations:["role", "game"]})
    if(! find ){
      find = nbRole
    }else{
      find.count = params.count;
    }
    this.roleGateway.changeNbRole(find);
    return this.nbRoleRepository.save(find);
  }


}
