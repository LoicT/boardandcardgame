import { Injectable } from '@nestjs/common';
import { Repository, Connection } from 'typeorm';
import { RoleInGame } from 'src/entities/roleInGame.entity';
import { Game } from 'src/entities/game.entity';
import { User } from 'src/entities/user.entity';
import { GameGateway } from './game.gateway';
import { MessageService } from 'src/message/message.service';

@Injectable()
export class GameService {

  gameRepository: Repository<Game>;
  roleInGameRepository: Repository<RoleInGame>;
  userRepository: Repository<User>;

  constructor(
    private connection: Connection,
    private gameGateway: GameGateway,
    private messageService: MessageService
    ) {
    this.gameRepository = connection.getRepository(Game);
    this.roleInGameRepository = connection.getRepository(RoleInGame);
    this.userRepository = connection.getRepository(User);
  }

  public async addUser(game:Game, user: User): Promise<RoleInGame> {
    if(!game.rolesingame){
      game.rolesingame = [];
    }
    if(game.rolesingame.length == 0){
      game.administrator = user;
    }
    let role = game.getRole(user);
    if(role == null){
      role = new RoleInGame();
      role.user = user;
      role.game = new Game(game.id);
      game.rolesingame.push(role);
      this.messageService.newMessage(user, game, "I join the game");
      this.gameGateway.joinGame(role);
      await this.roleInGameRepository.save(role);
    }else{
      this.messageService.newMessage(user, game, "I am back");
    }
    return role;
  }
}
