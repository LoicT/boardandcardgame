import { Controller, Get, Req, Param, Body, Post, Put, Delete } from '@nestjs/common';
import { Game } from '../entities/game.entity';
import { Repository, Connection } from 'typeorm';
import { Message } from '../entities/message.entity';
import { RoleInGame } from 'src/entities/roleInGame.entity';
import { User } from 'src/entities/user.entity';
import { GameGateway } from './game.gateway';
import { MessageService } from 'src/message/message.service';
import { RoleService } from 'src/role/role.service';
import { GameService } from './game.service';
import { NbRole } from 'src/entities/nbRole.entity';


@Controller('games')
export class GameController {

  gameRepository: Repository<Game>;
  roleInGameRepository: Repository<RoleInGame>;
  userRepository: Repository<User>;
  nbRoleRepository: Repository<NbRole>;

  constructor(
    private connection: Connection,
    private gameGateway: GameGateway,
    private messageService: MessageService,
    private roleService: RoleService,
    private gameService: GameService
    ) {
    this.gameRepository = connection.getRepository(Game);
    this.roleInGameRepository = connection.getRepository(RoleInGame);
    this.userRepository = connection.getRepository(User);
    this.nbRoleRepository = connection.getRepository(NbRole);
  }

  @Get()
  findAll(@Req() request) {
    return this.gameRepository.find();
  }

  @Get('/:id')
  async findById(@Param() params): Promise<Game> {
    let game = await this.gameRepository.findOne(params.id, {relations: ["administrator", "roles"]});
    if(game==null){
      return this.gameRepository.save(new Game(params.id));
    }
    return game;
  }

  @Get('/:id/messages')
  getMessages(@Param() params) {
    return this.connection.getRepository(Message).find({ relations: ["user"], where: { game: params['id'] } });
  }

  @Get('/:id/rolesInGame')
  getRolesInGame(@Param() params) {
    return this.connection.getRepository(RoleInGame).find({ relations: ["user", "role"], where: { game: params['id'] } });
  }

  @Get('/:id/roles')
  getRoles(@Param() params) {
    return this.connection.getRepository(NbRole).find({ relations: ["role"], where: { game: {id:params['id']} } });
  }

  @Post()
  addGame(@Req() request): Promise<Game> {
    if (request.body.game) {
      return this.gameRepository.save(request.body.game);
    } else {
      return this.gameRepository.save(new Game);
    }
  }

  @Post("/:id/start")
  async startGame(@Req() request){
    let game = await this.gameRepository.findOne(request.params.id, {relations:["administrator"]});
    game.started = true;
    game = await this.roleService.distributeRole(game);
    this.gameGateway.startGame(game)
    console.log(game.rolesingame)
    await this.roleInGameRepository.save(game.rolesingame);
    return this.gameRepository.save(game);
  }

  @Post("/:id/join/:idUser")
  async userJoinGame(@Req() request): Promise<Game>{
    let game = await this.gameRepository.findOne(request.params.id, {relations:["administrator"]});
    game.rolesingame = await this.roleInGameRepository.find({where: {game:game.id}, relations:["user", "game", "role"]});
    if(game == undefined){
      game = new Game(request.params.id);
    }
    let user = await this.connection.getRepository(User).findOne(request.params.idUser);
    if(user == undefined){
      user = new User()
      user.id = request.params.idUser;
    }
    await this.gameService.addUser(game, user);
    return this.gameRepository.save(game);
  }

  @Post("/:id/ready/:idUser")
  async userReady(@Req() request){
    const role:RoleInGame = await this.roleInGameRepository.findOne({where: {game:request.params.id, user:request.params.idUser}, relations:["user", "game"]});
    role.ready = true;
    this.roleInGameRepository.save(role);
    await this.messageService.newMessage(role.user, role.game, "I am ready");
    this.gameGateway.ready(role);
    return role;
  }

  @Put()
  async saveGame(@Req() req) {
    this.gameGateway.editGame(null, req.body);
    const res = await this.gameRepository.save(req.body)
    return res ;
  }

  @Delete("/:id/user/:idUser")
  async deleteUserFromGame(@Req() request){
    const gameCheck = await this.gameRepository.findOne(request.params.id);
    if(!gameCheck.started){
      this.roleInGameRepository.delete({game:request.params.id, user:request.params.idUser});
    }
    const user:User = await this.userRepository.findOne({where:{id:request.params.idUser}});
    const game = new Game(request.params.id);
    this.messageService.newMessage(user, game, "I quit the game");
    this.gameGateway.quitGame(user, game);
  }
}
