import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Client, Server } from 'socket.io';
import { Connection, Repository } from 'typeorm';
import { Game } from 'src/entities/game.entity';
import { User } from 'src/entities/user.entity';
import { RoleInGame } from 'src/entities/roleInGame.entity';
import { MessageGateway } from 'src/message/message.gateway';
import { MessageService } from 'src/message/message.service';

@WebSocketGateway()
export class GameGateway {
  gameRepository: Repository<Game>;
  roleRepository: Repository<RoleInGame>;

  @WebSocketServer()
  server: Server;

  constructor(
    private connection: Connection,
    private messageGateway: MessageGateway,
    private messageService: MessageService
    ) {
    this.gameRepository = connection.getRepository(Game);
    this.roleRepository = connection.getRepository(RoleInGame);
  }

  @SubscribeMessage('join')
  async joinGame(role: RoleInGame) {
    this.server.emit('hasJoin', role);
  }

  @SubscribeMessage("connect")
  async connect(client: Client, data:any){
    return;
  }

  @SubscribeMessage('ready')
  async ready(role:RoleInGame) {
    this.server.emit("isReady", role);
  }

  @SubscribeMessage('quit')
  async quitGame(user: User, game:Game) {
    let role = new RoleInGame(user, game);
    this.server.emit('hasQuit', role);
  }

  @SubscribeMessage('editGame')
  async editGame(client:Client, data:Game){
    this.server.emit('editGame', data);
  }

  startGame(game: Game) {
    this.server.emit("startGame", game);
  }
}
