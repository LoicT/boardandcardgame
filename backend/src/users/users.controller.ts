import { Controller, Get, Req, Param, Post, Body } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { User } from '../entities/user.entity';
import { Message } from 'src/entities/message.entity';
import { RoleInGame } from 'src/entities/roleInGame.entity';

@Controller('users')
export class UsersController {

	repository: Repository<User>;

	constructor(private connection:Connection){
		this.repository = connection.getRepository(User);
	}

	@Get()
	findAll(@Req() request){
		if(request.query["username"])
			return this.repository.findOne({where:{username:request.query["username"]}});
		else
			return this.repository.find();
	}

	@Get('/:id')
	findById(@Param() params): Promise<User>{
		return this.repository.findOne(params.id);
	}

	@Get('/:id/messages')
	getMessages(@Param() params){
		return this.connection.getRepository(Message).find({where:{user:params['id']}}); 
  }
  
  @Get('/:id/role/:idGame')
  getRoleInGame(@Param() params){
    return this.connection.getRepository(RoleInGame).findOne({where:{game:params['idGame'], user:params['id']}});
  }

	@Post()
	async addUser(@Req() request, @Body() user:User): Promise<User>{
		var userTemp = await this.repository.findOne({where:{username:user.username}});
		if(userTemp){
			return userTemp;
		}
		return this.repository.save(user);
	}
	
}
