import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { IoAdapter } from '@nestjs/websockets';

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
  app.enableCors({origin:"http://localhost:4200", credentials:true});
  //app.useWebSocketAdapter(new IoAdapter());
	await app.listen(3000);
}
bootstrap();
 