import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersController } from './users/users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LoginController } from './login/login.controller';
import { GameController } from './game/game.controller';
import { MessageController } from './message/message.controller';
import { MessageGateway } from './message/message.gateway';
import { GameGateway } from './game/game.gateway';
import { MessageService } from './message/message.service';
import { RoleService } from './role/role.service';
import { RoleGateway } from './role/role.gateway';
import { RoleController } from './role/role.controller';
import { GameService } from './game/game.service';

import 'reflect-metadata';

@Module({
	imports: [
		TypeOrmModule.forRoot({
			type: 'mysql',
			host: 'localhost',
			port: 3306,
			username: 'root',
			password: 'root',
			database: 'loupgarou',
			entities: [__dirname + '/**/*.entity{.ts,.js}'],
			synchronize: true,
		  }),
	],
	controllers: [
    AppController, 
    UsersController, 
    LoginController, 
    GameController, 
    MessageController, RoleController
  ],
  providers: [AppService, MessageGateway, GameGateway, MessageService, RoleService, RoleGateway, GameService]
})
export class AppModule { }
