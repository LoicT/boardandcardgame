import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, OneToMany, ManyToOne } from 'typeorm';
import { RoleInGame } from './roleInGame.entity';
import { NbRole } from './nbRole.entity';
import { VictoryCondition } from './victoryCondition.entity';

@Entity()
export class Role {

  constructor(id?:number){
    this.id = id;
  }

	@PrimaryGeneratedColumn()
	id: number;

	@ManyToMany(type=>RoleInGame, role=>role.role)
  rolesingame:RoleInGame[];
  
  @OneToMany(type=>NbRole, roles=>roles.role)
  nbRole: NbRole[];

  @ManyToOne(type=>VictoryCondition, condition=>condition.role)
  victoryCondition: VictoryCondition;

	@Column({ length: 100 })
	name: string;

	@Column({length: 200})
	description: string;

	@Column()
	priority: number;

}