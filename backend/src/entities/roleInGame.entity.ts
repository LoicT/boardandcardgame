import { Entity, ManyToOne, Column } from 'typeorm';
import 'reflect-metadata';
import { User } from './user.entity';
import { Game } from './game.entity';
import { Role } from './role.entity';


@Entity()
export class RoleInGame {

  constructor(
    user?:User,
    game?:Game
  ){
    this.user = user;
    this.game = game;
  }

	@ManyToOne(type=> User, user=>user.rolesingame, {
        primary: true,
        nullable: false,
	})	
	user: User;

	@ManyToOne(type=>Game, game=>game.rolesingame, {
        primary: true,
        nullable: false,
	})
	game: Game;

	@ManyToOne(type=>Role, role=>role.rolesingame)
  role: Role;
  
  @Column()
  ready: boolean = false;

  @Column()
  alive: boolean = true;
}