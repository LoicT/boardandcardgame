import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, OneToMany, ManyToOne } from 'typeorm';
import { Role } from './role.entity';

@Entity()
export class VictoryCondition {

  constructor(){}

	@PrimaryGeneratedColumn()
	id: number;

	@OneToMany(type=>Role, role=>role.victoryCondition)
  role:Role[];

	@Column({length: 200})
	description: string;

}