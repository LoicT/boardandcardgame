import { Entity, Column, ManyToOne } from 'typeorm';
import { Role } from './role.entity';
import { Game } from './game.entity';

@Entity()
export class NbRole {
  constructor(
    role?:Role,
    game?:Game,
    count?:number
  ){
    this.role = role;
    this.game = game;
    this.count = count;
  }

  @ManyToOne(type=>Role, role=>role.nbRole, {primary:true})
  role: Role;

  @ManyToOne(type=>Game, game=>game.roles, {primary:true})
  game: Game;
  
  @Column()
  count:number = 0;
}