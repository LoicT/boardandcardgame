import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, ManyToMany, OneToMany } from 'typeorm';
import { Message } from './message.entity';
import { RoleInGame } from './roleInGame.entity';
import { Game } from './game.entity';

@Entity()
export class User {
	@PrimaryGeneratedColumn()
	id: number;

	@Column({ length: 100 })
	username: string;

	@OneToMany(type=>Message, message=>message.user)
	messages: Message[];

	@OneToMany(type=>RoleInGame, role=>role.user)
  rolesingame:RoleInGame[];
  
  @OneToMany(type=>Game, game=>game.administrator)
  gameAdministred: Game[];
}