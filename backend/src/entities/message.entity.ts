import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Game } from './game.entity';
import { User } from './user.entity';

@Entity()
export class Message {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 1000 })
  text: string;

  @ManyToOne(type => Game, game => game.messages)
  game: Game;

  @ManyToOne(type => User, user => user.messages)
  user: User;
}
