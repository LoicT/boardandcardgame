import { Entity, PrimaryGeneratedColumn, OneToMany, Column, ManyToOne } from 'typeorm';
import { Message } from './message.entity';
import { RoleInGame } from './roleInGame.entity';
import { User } from './user.entity';
import { NbRole } from './nbRole.entity';

@Entity()
export class Game {
  constructor(id?: number){
    this.id = id;
  }

	@PrimaryGeneratedColumn()
	id: number;

	@OneToMany(type=>Message, message=>message.game)
	messages: Message[];

	@OneToMany(type=>RoleInGame, roleingame=>roleingame.game)
  rolesingame: RoleInGame[];

  @ManyToOne(type=>User, user=>user.gameAdministred)
  administrator: User;

  @OneToMany(type=>NbRole, roles=>roles.game)
  roles: NbRole[];

  @Column()
  started: boolean = false;

  public getNbPlayer(): number {
    return this.rolesingame.length;
  }

  public getRole(user:User): RoleInGame{
    for (const roleInGame of this.rolesingame) {
      if(roleInGame.user.id == user.id){
        return roleInGame;
      }
    }
    return null;
  }
}