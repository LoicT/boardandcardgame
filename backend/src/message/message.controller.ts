import { Controller, Get, Req, Param, Post } from '@nestjs/common';
import { Message } from '../entities/message.entity';
import { Connection, Repository } from 'typeorm';
import { User } from 'src/entities/user.entity';
import { Game } from 'src/entities/game.entity';
import { MessageGateway } from './message.gateway';

@Controller('messages')
export class MessageController {

	repository: Repository<Message>;

	constructor(
    private connection: Connection,
    private messageGateway: MessageGateway
    ) {
		this.repository = connection.getRepository(Message);
	}

	@Get()
	async findAll(@Req() request) {
		return this.repository.find({relations:["user"]});
	}

	@Get('/:id')
	findById(@Param() params): Promise<Message> {
		return this.repository.findOne(params.id);
	}

	@Get('/:id/user')
	async findByIdUser(@Param() params) {
		return await this.repository.find({where:{user:params.id}, relations:["user"]});
  }

  @Post()
  postMessage(@Req() request){
    this.messageGateway.emitMessage(request.body);
    return this.repository.save(request.body);
  }
}
