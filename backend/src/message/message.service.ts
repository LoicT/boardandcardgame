import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { Message } from 'src/entities/message.entity';
import { MessageGateway } from './message.gateway';
import { Game } from 'src/entities/game.entity';
import { User } from 'src/entities/user.entity';

@Injectable()
export class MessageService {

	repository: Repository<Message>;

	constructor(
    private connection: Connection,
    private messageGateway: MessageGateway
    ) {
		this.repository = connection.getRepository(Message);
	}
  
  public async newMessage(user: User, game: Game, text:string){
    const message = new Message();
    message.user = user;
    message.game = game;
    message.text = text;
    await this.messageGateway.emitMessage(message);
    await this.repository.save(message);
  }
}
