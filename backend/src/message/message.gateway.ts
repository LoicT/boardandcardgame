import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Client, Server } from 'socket.io';
import { Connection, Repository } from 'typeorm';
import { Message } from 'src/entities/message.entity';

@WebSocketGateway()
export class MessageGateway {

  repository: Repository<Message>;

  @WebSocketServer()
  server: Server;

  constructor(private connection: Connection) {
    this.repository = connection.getRepository(Message);
  }

  @SubscribeMessage('newMessage')
  async newMessage(client: Client, data: {}) {
    return;
  }

  async emitMessage(message:Message){
    await this.server.emit('newMessage', message);
  }
}
