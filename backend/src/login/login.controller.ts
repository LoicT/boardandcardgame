import { Controller, Get } from '@nestjs/common';
import { Repository, Connection } from 'typeorm';
import { User } from '../entities/user.entity';

@Controller('login')
export class LoginController {

	repository: Repository<User>;

	constructor(private connection:Connection){
		this.repository = connection.getRepository(User);
	}

}
