export class User {
  id: number;
  username: string;
	messages: Message[];
	rolesingame:RoleInGame[];
}

export class Message {
  constructor(
    public text = "",
    public user?: User,
    public game?: Game
  ){}
  public id: number;
}

export class Game {
	id: number;
	messages: Message[];
  rolesingame: RoleInGame[];
  roles: NbRole[];
  started:boolean;
  administrator: User;
}

export class Role {
	id: number;
	name: string;
	description: string;
	priority: number;
	rolesingame:RoleInGame[];
}

export class RoleInGame {
  constructor(
    public user: User,
    public game: Game
  ){}
  role: Role;
  ready:boolean;
  alive:boolean;
}

export class NbRole{
  constructor(
    public game:Game,
    public role: Role,
    public count?:number
  ){}
}