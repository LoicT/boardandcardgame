import { Injectable } from '@angular/core';
import { Constants } from 'src/app/Constants';
import { Message } from 'src/entity/entities';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

	MESSAGEAPI_URL:string = Constants.URL_API + "/messages";

	constructor(
		private http: HttpClient
	) { }

	getMessage(id: number): any {
		return this.http.get(this.MESSAGEAPI_URL+"/" + id);
	}

	sendMessage(message:Message){
		return this.http.post(this.MESSAGEAPI_URL, message);
	}

	getUser(message: Message): any {
		return this.http.get(this.MESSAGEAPI_URL+"/" + message.id + "/user");
	}
}
