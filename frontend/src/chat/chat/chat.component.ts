import { Component, OnInit, Input } from '@angular/core';
import { ChatService } from '../chat.service';
import { User, Message, Game, RoleInGame } from 'src/entity/entities';
import { Observable } from 'rxjs';
import { MessageService } from '../message.service';
import { GameService } from 'src/loup-garou/game.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  @Input()
  public game: Game;

  @Input()
  public user: User;

  @Input()
  public roleInGame: RoleInGame;

  message: Message = new Message;

  constructor(
    private chatService: ChatService
    ) {
  }

  async ngOnInit() {
    this.chatService 
      .getMessages()
      .subscribe((message:Message) => {
        if (this.game.messages && message.game.id == this.game.id) {
          this.game.messages.push(message);
        }
      });
  }

  async sendMessage() {
    this.message.user = this.user;
    this.message.game = this.game;
    await this.chatService.sendMessage(this.message).toPromise();
    this.message.text = '';
  }

}

