import { Injectable } from '@angular/core';

import * as io from 'socket.io-client';
import { Observable } from 'rxjs';
import { User, Message } from 'src/entity/entities';
import { Constants } from 'src/app/Constants';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private url = Constants.URL_API;

  private MESSAGE_API = Constants.URL_API + "/messages";
  private socket;

  constructor(
    private http: HttpClient
  ) {
    this.socket = io(this.url);
  }

  public sendMessage(message:Message) {
    this.socket.emit("newMessage", {});
    return this.http.post<Message>(this.MESSAGE_API, message);
  }

  public getMessages = () => {
    return Observable.create((observer) => {
      this.socket.on('newMessage', (message) => {
        observer.next(message);
      });
    });
  }
}
