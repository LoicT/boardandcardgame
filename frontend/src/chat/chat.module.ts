import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatComponent } from './chat/chat.component';
import { EntityModule } from '../entity/entity.module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ChatService } from './chat.service';

@NgModule({
  declarations: [ChatComponent],
  imports: [
    CommonModule,
    EntityModule,
    FormsModule,
    RouterModule
  ],
  exports: [
    ChatComponent
  ],
  providers:[
    ChatService
  ]
})
export class ChatModule { }
