import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Game, Message, User, RoleInGame, NbRole } from 'src/entity/entities';
import { HttpClient } from '@angular/common/http';
import { Constants } from 'src/app/Constants';
import * as io from 'socket.io-client';

@Injectable({
	providedIn: 'root'
})
export class GameService {

  private socket;

	GAMEAPI_URL:string = Constants.URL_API + "/games";

	constructor(
		private http: HttpClient
	) { 
    this.socket = io(Constants.URL_API);
  }

	createGame(game:Game): Observable<Game> {
		return this.http.post<Game>(this.GAMEAPI_URL, game);
	}

	getGame(id: number): Observable<Game> {
		return this.http.get<Game>(this.GAMEAPI_URL+"/" + id);
	}

	getMessages(game: Game): Observable<Message[]> {
		return this.http.get<Message[]>(this.GAMEAPI_URL + "/" + game.id + "/messages");
  }

  getRolesInGame(game: Game): Observable<RoleInGame[]> {
		return this.http.get<RoleInGame[]>(this.GAMEAPI_URL + "/" + game.id + "/rolesInGame");
  }

  editGame(game:Game){
    return this.http.put<Game>(this.GAMEAPI_URL, game);
  }

  userJoinGame(user: User, game: Game): Observable<Game> {
    return this.http.post<Game>(this.GAMEAPI_URL + "/" + game.id + "/join/" + user.id, null);
  }

  ready(user: User, game: Game): Observable<RoleInGame> {
    return this.http.post<RoleInGame>(this.GAMEAPI_URL + "/" + game.id + "/ready/" + user.id, null);
  }

  startGame(game: Game): Observable<Game> {
    return this.http.post<Game>(this.GAMEAPI_URL + "/" + game.id + "/start", null);
  }

  quitGame(user: User, game: Game): Observable<any> {
    return this.http.delete<any>(this.GAMEAPI_URL + "/" + game.id + "/user/" + user.id);
  }

  getNbRoles(game: Game): Observable<NbRole[]> {
    return this.http.get<NbRole[]>(this.GAMEAPI_URL + "/" + game.id + "/roles");
  }
  
  connect(): any {
    this.socket.emit("connect", {});
  }

  getEditGame (): Observable<Game> {
    return Observable.create((observer) => {
      this.socket.on('editGame', (game: Game) => {
        observer.next(game);
      });
    });
  }

  getJoinUser(): Observable<RoleInGame> {
    return Observable.create((observer) => {
      this.socket.on('hasJoin', (user: RoleInGame) => {
        observer.next(user);
      });
    });
  }

  getQuitUser(): Observable<RoleInGame> {
    return Observable.create((observer) => {
      this.socket.on('hasQuit', (user: RoleInGame) => {
        observer.next(user);
      });
    });
  }

  getReadyUser (): Observable<RoleInGame> {
    return Observable.create((observer) => {
      this.socket.on('isReady', (role:RoleInGame) => {
        observer.next(role);
      });
    });
  }

  getStartGame(): Observable<Game> {
    return Observable.create((observer) => {
      this.socket.on('startGame', (game:Game) => {
        observer.next(game);
      });
    });
  }

  getRoleInGame(game: Game, user: User): RoleInGame {
    for (const roleInGame of game.rolesingame) {
      if(roleInGame.user.id==user.id){
        return roleInGame;
      }
    }
    return null;
  }

  public getParticipantNeeded(game:Game): number{
    if(!game.roles){
      return 0;
    }
    let res: number = 0
    for (const role of game.roles) {
      const count:number = role.count;
      res += +role.count;
    }
    return res;
  };
}
