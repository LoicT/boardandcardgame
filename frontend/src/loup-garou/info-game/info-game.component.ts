import { Component, OnInit, Input } from '@angular/core';
import { Game, User, RoleInGame, Role, NbRole } from 'src/entity/entities';
import { GameService } from '../game.service';
import { RoleService } from '../role.service';

@Component({
  selector: 'app-info-game',
  templateUrl: './info-game.component.html',
  styleUrls: ['./info-game.component.css']
})
export class InfoGameComponent implements OnInit {

  @Input()
  game:Game;

  @Input()
  user:User;

  @Input()
  nbReady: number;

  @Input()
  isAdmin: boolean;

  @Input()
  public roleInGame: RoleInGame;

  @Input()
  public playerNeeded:number;

  constructor(
    private gameService:GameService,
    private roleService: RoleService
  ) { }

  async ngOnInit() {
    this.listenJoinUser();
    this.listenQuitUser();
  }

  listenQuitUser(){
    this.gameService
      .getQuitUser()
      .subscribe((data: RoleInGame) => {
        if (data.game.id === this.game.id) {
          for (let index = 0; index < this.game.rolesingame.length; index++) {
            if(this.game.rolesingame[index].user.id === data.user.id){
              this.game.rolesingame.splice(index, 1)
            }
          }
        }
      });
  }

  listenJoinUser(){
    this.gameService
      .getJoinUser()
      .subscribe((data: RoleInGame) => {
        if (this.game.rolesingame && data.game.id === this.game.id) {
          this.game.rolesingame.push(data)
        }
      });
  }

  async ready(){
    this.roleInGame = await this.gameService.ready(this.user, this.game).toPromise();
  }

  async startGame(){
    this.game = await this.gameService.startGame(this.game).toPromise();
  }

  async updateRole(role: NbRole){
    role.game = this.game;
    await this.roleService.saveNbRole(role).toPromise();
  }

  test(){
    console.log(this.game, this.user, this.roleInGame);
  }

}
