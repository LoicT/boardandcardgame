import { Component, OnInit, OnDestroy } from '@angular/core';
import { Game, User, Message, RoleInGame, NbRole, Role } from 'src/entity/entities';
import { LoginService } from 'src/login/login.service';
import { GameService } from '../game.service';
import { ActivatedRoute, Params } from '@angular/router';
import { ChatService } from 'src/chat/chat.service';
import { UserService } from 'src/login/user.service';
import { RoleService } from '../role.service';

@Component({
  selector: 'app-loup-garou',
  templateUrl: './loup-garou.component.html',
  styleUrls: ['./loup-garou.component.css']
})
export class LoupGarouComponent implements OnInit, OnDestroy {

  public game: Game = new Game();

  public user: User = new User();

  public roleInGame: RoleInGame;

  public isAdmin: boolean = false;

  public playerNeeded: number = 0;

  nbReady: number = 0;

  constructor(
    private loginService: LoginService,
    private gameService: GameService,
    private route: ActivatedRoute,
    private roleService: RoleService
    ) { }

  async ngOnInit() {
    this.initSubscribe();
    const params = await this.route.params.subscribe(async (params:Params)=>{
      this.user = await this.loginService.getUser();
      this.game = await this.gameService.getGame(params.id).toPromise();
      this.game = await this.gameService.userJoinGame(this.user,this.game).toPromise();
      this.game.roles = await this.getNbRoles();
      this.playerNeeded = this.gameService.getParticipantNeeded(this.game);
      this.game.messages = await this.gameService.getMessages(this.game).toPromise();
      this.isAdmin = this.game.administrator.id == this.user.id;
      this.roleInGame = this.gameService.getRoleInGame(this.game, this.user);
      this.initNbReady();
    });
  }

  initSubscribe():void{
    this.gameService.connect();
    this.gameService
    .getReadyUser()
    .subscribe(()=>{
      this.nbReady++;
    });
    this.gameService
      .getQuitUser()
      .subscribe((data: RoleInGame)=>{
        this.game.rolesingame.splice(this.game.rolesingame.indexOf(data), 1);
        this.initNbReady();
      });
    this.gameService
      .getEditGame()
      .subscribe((game:Game)=>{
        if(game.id === this.game.id){
          this.game = game;
        }
      });
    this.roleService
      .getNbRoleUpdate()
      .subscribe((data:NbRole)=>{
        if(data.game.id == this.game.id){
          for (const nbRole of this.game.roles) {
            if(nbRole.role.id == data.role.id){
              nbRole.count = data.count;
              this.playerNeeded = this.gameService.getParticipantNeeded(this.game);
              return;
            }
          }
        }
      });
    this.gameService
      .getStartGame()
      .subscribe((data:Game)=>{
        if(data.id == this.game.id){
          this.game.started = true;
          this.game.rolesingame = data.rolesingame;
          this.roleInGame = this.gameService.getRoleInGame(this.game, this.user);
          console.log(this.game)
        }
      });
  }

  async ngOnDestroy() {
    await this.gameService.quitGame(this.user, this.game).toPromise();
  }

  initNbReady(): void{
    this.nbReady = 0;
    for (const role of this.game.rolesingame) {
      if(role.ready){
        this.nbReady++;
      }
    }
  }
  
  async getNbRoles(): Promise<NbRole[]> {
    const nbroles: NbRole[] = await this.gameService.getNbRoles(this.game).toPromise();
    const roles : Role[] = await this.roleService.getAllRole().toPromise();
    const res: NbRole[] = [];
    for (const role of roles) {
      let contains = false;
      for (const nbRole of nbroles) {
        if(nbRole.role.id==role.id){
          contains=true;
          res.push(nbRole);
          break;
        }
      }
      if(!contains){
        res.push(new NbRole(this.game, role, 0));
      }
    }
    return res;
  }

}
