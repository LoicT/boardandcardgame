import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoupGarouComponent } from './loup-garou.component';

describe('LoupGarouComponent', () => {
  let component: LoupGarouComponent;
  let fixture: ComponentFixture<LoupGarouComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoupGarouComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoupGarouComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
