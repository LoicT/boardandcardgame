import { Injectable } from '@angular/core';
import { Constants } from 'src/app/Constants';
import { HttpClient } from '@angular/common/http';
import { NbRole, Role } from 'src/entity/entities';
import { Observable } from 'rxjs';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  private socket;

	ROLEAPI_URL:string = Constants.URL_API + "/roles";

	constructor(
		private http: HttpClient
	) { 
    this.socket = io(Constants.URL_API);
  }

  getAllRole():Observable<Role[]>{
    return this.http.get<Role[]>(this.ROLEAPI_URL);
  }

  saveNbRole(role: NbRole): Observable<NbRole> {
    return this.http.post<NbRole>(this.ROLEAPI_URL + "/" + role.role.id + "/game/" + role.game.id + "/count/" + role.count, null);
  }

  getNbRoleUpdate (): Observable<NbRole> {
    return Observable.create((observer) => {
      this.socket.on('changeNbRole', (role:NbRole) => {
        observer.next(role);
      });
    });
  }
}
