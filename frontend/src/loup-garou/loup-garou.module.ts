import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoupGarouComponent } from './loup-garou/loup-garou.component';
import { ChatModule } from 'src/chat/chat.module';
import { InfoGameComponent } from './info-game/info-game.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [LoupGarouComponent, InfoGameComponent],
  imports: [
    CommonModule,
    ChatModule,
    FormsModule
  ]
})
export class LoupGarouModule { }
