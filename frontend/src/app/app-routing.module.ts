import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Constants } from './Constants';
import { ChooseGameComponent } from './choose-game/choose-game.component';
import { LoupGarouComponent } from 'src/loup-garou/loup-garou/loup-garou.component';
import { LoginComponent } from 'src/login/login/login.component';

const routes: Routes = [
  { path: '', redirectTo: Constants.LOGIN_URL, pathMatch: 'full' },
  { path: Constants.LOUP_GAROU_URL + '/:id', component: LoupGarouComponent },
  { path: Constants.LOUP_GAROU_URL, component: LoupGarouComponent },
  { path: Constants.LOGIN_URL, component: LoginComponent },
  { path: Constants.CHOOSEGAME_URL, component: ChooseGameComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
