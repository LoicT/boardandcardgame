import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ChooseGameComponent } from './choose-game/choose-game.component';
import { LoupGarouModule } from 'src/loup-garou/loup-garou.module';
import { LoginModule } from '../login/login.module'
import { HttpClientModule } from '@angular/common/http'

@NgModule({
  declarations: [
    AppComponent,
    ChooseGameComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LoupGarouModule,
    FormsModule,
    LoginModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
