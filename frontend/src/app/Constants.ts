export class Constants {

  // API Backend
  static readonly PORT_BACK = 3000;
  static readonly URL_API: string = 'http://localhost:' + Constants.PORT_BACK;

  // Routeur Frontend
  static readonly LOGIN_URL = 'login';
  static readonly CHOOSEGAME_URL = 'choose-game';

  static readonly LOUP_GAROU_URL = 'loupgarou';

}
