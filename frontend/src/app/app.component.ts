import { Component } from '@angular/core';
import { LoginService } from 'src/login/login.service';
import { Router } from '@angular/router';
import { Constants } from './Constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'boardandcardgame';

  constructor(
    private loginService:LoginService,
    private router: Router
  ){}

  public logout(): void{
    this.loginService.logout();
    this.router.navigateByUrl(Constants.LOGIN_URL);
  }
}
