import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Constants } from '../Constants';

@Component({
  selector: 'app-choose-game',
  templateUrl: './choose-game.component.html',
  styleUrls: ['./choose-game.component.css']
})
export class ChooseGameComponent implements OnInit {

  idpartie: number;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  joinGame(game: string, idPartie: number) {
    let urlGame: string;
    idPartie !== undefined ? urlGame = Constants.LOUP_GAROU_URL + '/' + idPartie : urlGame = Constants.LOUP_GAROU_URL;
    this.router.navigateByUrl(urlGame);
  }

}
