import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from 'src/app/Constants';
import { User } from 'src/entity/entities';
import { UserService } from './user.service';
import { Router } from '@angular/router';


@Injectable({
	providedIn: 'root'
})
export class LoginService {

	constructor(
    private http:HttpClient,
    private userService: UserService,
    private router : Router
    ) { }

	loginAPI:string = Constants.URL_API + "/login";

	login(user: User):void{
		localStorage.setItem('user', JSON.stringify(user));
	}

	async getUser(): Promise<User>{
    let user = JSON.parse(localStorage.getItem('user'));
    if(user == null){
      this.logout();
      return;
    }
    user = await this.userService.getById(user.id).toPromise();
		return user;
	}

	logout():void {
    localStorage.removeItem('user');
    this.router.navigateByUrl(Constants.LOGIN_URL);
	}
}
