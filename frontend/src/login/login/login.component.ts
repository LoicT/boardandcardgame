import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/entity/entities';
import { Constants } from 'src/app/Constants';
import { UserService } from '../user.service';
import { LoginService } from '../login.service';


@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	user:User = new User();

	constructor(
		private userService: UserService,
		private loginService: LoginService,
		private router: Router
	) { }

	async ngOnInit() {
    this.loginService.logout();
    console.log(await this.loginService.getUser());
		if(await this.loginService.getUser()!= undefined){
			this.router.navigateByUrl(Constants.CHOOSEGAME_URL);
		}
	}

	async login(){
		await this.userService.getByUsername(this.user.username).subscribe(async (data:User[])=>{
			var userTemp;
			if(data=[]){
				userTemp = await this.createUser();
			}
			else
				userTemp = data[0];
			this.loginService.login(userTemp);
			this.router.navigateByUrl(Constants.CHOOSEGAME_URL);
		})
	}	

	async createUser(){
		return await this.userService.createUser(this.user).toPromise();
	}

}
