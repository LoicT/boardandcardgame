import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Constants } from 'src/app/Constants';
import { Observable } from 'rxjs';
import { User, Game, RoleInGame } from 'src/entity/entities';

@Injectable({
	providedIn: 'root'
})
export class UserService {

	constructor( 
		private http: HttpClient
	) { }

	userAPI:string = Constants.URL_API + "/users";

	getAll(){
		return this.http.get(this.userAPI, {headers: {'Content-Type' : 'application/json'}});
	}

	getById(id:number): Observable<User>{
		return this.http.get<User>(this.userAPI+ "/" + id);
	}

	getByUsername(username:string): Observable<User[]>{
		const options = username ?
		{ params: new HttpParams().set('username', username), headers: {'Content-Type' : 'application/json', 'Access-Control-Allow-Origin' : '*'}}: {};
		return this.http.get<User[]>(this.userAPI, options);
	}

	createUser(user: User): any {
		return this.http.post(this.userAPI, user);
  }
  
  getRoleInGame(user:User, game:Game): Observable<RoleInGame>{
    return this.http.get<RoleInGame>(this.userAPI+ "/" + user.id + "/role/" + game.id);
  }

}
